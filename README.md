## 1、安装webpack

（1）全局：```npm install -g webpack```

（2）项目文件夹（cmd跳转到项目的文件夹）：```npm install --save-dev webpack```


## 2、package.json（一直按enter键）
```
npm init
```

## 3、安装loader

（1）```npm install --save-dev style-loader css-loader```

（2）```npm install --save-dev url-loader```


## 4、webpack.config.js
```
var webpack = require('webpack');

module.exports = {
    entry: __dirname+'/app/main.js',

    output: {
        path: __dirname+'/public',
        filename: 'bundle.js'
    },

    module: {
        loaders: [
            {test: /\.css$/,loader: 'style-loader!css-loader'},
            {test: /\.(png|jpg|gif)$/, loader: 'url-loader?limit=8192'}
        ]
    }
};
```

## 5、main.js
```
require('./main.css');
require('./Greeter.js');
```


## 6、index.html
```
<!-- index.html -->
<html>
<head>
  <meta charset="utf-8">
</head>
<body>
  <script src="bundle.js"></script>
</body>
</html>
```


## 7、目录图：

![输入图片说明](https://git.oschina.net/uploads/images/2017/1002/133848_41347db5_1036569.png "171518_rH2i_2941696.png")

